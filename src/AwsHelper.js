import AWS from "aws-sdk"
const EXPORTS_MAP = {}

const AwsHelper = (credentials) => {
    AWS.config.update({
        "accessKeyId": credentials["accessKey"],
        "secretAccessKey": credentials["secretKey"],
        "region": credentials["region"]
    });
    AWS.config.getCredentials(function(err) {
        if (err) console.log(err.stack);
        // credentials not loaded
        else {
            let cloudformation = new AWS.CloudFormation();
            cloudformation.listExports({}, function(err, data) {
               if (err) {

               } else {

                   data["Exports"].forEach(elem => {
                       EXPORTS_MAP[elem.Name] = elem.Value
                   })
               }
            });
        }
    });
    return EXPORTS_MAP
};

export default AwsHelper
