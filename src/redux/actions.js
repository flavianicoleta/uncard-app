import { API } from "aws-amplify";

export const GET_DATA = 'GET_DATA'

export function getData(data) {
    return {
        type: GET_DATA,
        data: data
    }
}

export function fetchData() {
    return dispatch => {

        // console.log('in fetch data')
        const apiName = 'TestGatewayAPI';
        const path = '/uncard';

        return API.get(apiName, path, {
            headers:
                {
                    'Content-Type': 'application/json'
            //         'Access-Control-Allow-Origin': '*',
            // 'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key'
                }
        })
            .then(response => dispatch(getData(response)))
    }
}