import {createStore, applyMiddleware, combineReducers} from 'redux'
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import selectedData from "./reducers";

const loggerMiddleware = createLogger()

const reducer = combineReducers({
    selectedData
})

export const store = createStore(
    reducer,
    applyMiddleware(thunkMiddleware, loggerMiddleware)
)