import {
    GET_DATA
} from './actions'

function selectedData(state = {}, action) {
    switch (action.type) {
        case GET_DATA:
            return {
                myData: action.data.message
            }
        default:
            return state
    }
}

export function getMyData(state) {
    return state.myData
}


export default selectedData