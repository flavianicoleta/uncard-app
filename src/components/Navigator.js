import React, { Component } from 'react';
import { Navbar, Nav, BSpan } from 'bootstrap-4-react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import {store} from '../redux/store'
import {Provider} from "react-redux";
import {Home, NewHome} from "./index";
import {AmplifySignOut} from "@aws-amplify/ui-react";


export default class Navigator extends Component {
    render() {
        return (
            <Navbar expand="md" dark bg="dark" fixed="top">
                <Navbar.Brand href="#">Journal</Navbar.Brand>
                <Navbar.Toggler target="#navbarsExampleDefault" />

                <Navbar.Collapse id="navbarsExampleDefault">
                    <Navbar.Nav mr="auto">
                        <Provider store={store}>
                            <Router>
                                <Switch>
                                    <Route exact path="/" component={Home} />
                                    <Route exact path="/new" component={NewHome} />
                                </Switch>
                            </Router>
                        </Provider>

                    </Navbar.Nav>
                    <Navbar.Text>Greetings</Navbar.Text>
                    <AmplifySignOut className='sign-out'/>
                </Navbar.Collapse>
            </Navbar>
        )
    }
}
