import React, { Component } from 'react';
import { Container } from 'bootstrap-4-react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import {Home, Login, NewHome, Navigator} from "./index";
import { Provider } from 'react-redux'
import {store} from '../redux/store'

export default class Main extends Component {
    render() {
        return (
            <React.Fragment>
                {/*<Navigator/>*/}
                <Container as="main" role="main">
                    <div className="starter-template">
                        <Provider store={store}>
                            <Router>
                                <Switch>
                                    <Route exact path="/" component={Home} />
                                    <Route exact path="/new" component={NewHome} />
                                </Switch>
                            </Router>
                        </Provider>
                    </div>
                </Container>
            </React.Fragment>
        )
    }
}
