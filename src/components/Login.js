import React, { Component } from 'react';
import {
    AmplifySignUp,
    AmplifyAuthenticator,
    AmplifySignIn
} from '@aws-amplify/ui-react';

export default class Login extends Component {
    render() {
        return (
            <AmplifyAuthenticator>
                <AmplifySignUp
                    slot='sign-up'
                    usernameAlias='email'
                    formFields={[
                        {
                            type: 'name',
                            label: 'Name',
                            placeholder: 'Name',
                            required: true,
                        },
                        {
                            type: 'email',
                            label: 'Email',
                            placeholder: 'name@host.com',
                            required: true,
                        },
                        {
                            type: 'password',
                            label: 'Password',
                            placeholder: '******',
                            required: true,
                        }

                    ]}
                />
                <AmplifySignIn slot='sign-in' usernameAlias='email'/>
            </AmplifyAuthenticator>
        )
    }
}
