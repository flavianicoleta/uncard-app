import React, { Component } from 'react';
import { fetchData } from '../redux/actions'
import selectedData, { getMyData } from '../redux/reducers'
import { connect } from 'react-redux'
import {AmplifySignOut} from "@aws-amplify/ui-react";

class Home extends Component {

    componentDidMount() {
        this.props.dispatch(fetchData())
    }

    render() {
        console.log(this.props.myData2)
        return (

            <div>
                <AmplifySignOut className='sign-out'/>
                <h1>MyData: {this.props.myData2}</h1>
            </div>

        )
    }

}
const mapStateToProps = state => ({
    myData: getMyData(state),
    myData2: state.selectedData.myData
})
export default connect(mapStateToProps)(Home)