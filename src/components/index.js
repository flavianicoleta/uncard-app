export { default as Login } from './Login';
export { default as Home } from './Home';
export { default as NewHome } from './NewHome';
export { default as Navigator } from './Navigator';
export { default as Main } from './Main';