import Amplify from "@aws-amplify/core";
import AwsHelper from "./AwsHelper";

const ACCESS_KEY = "AKIAURY6SL6VQMLLJ37L";
const SECRET_KEY = "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";
// const USER_POOL_ID = 'UncardUserPoolId';
// const USER_POOL_WEB_CLIENT_ID = 'UncardUserPoolWebClientId';
// const REST_API = 'UncardRestApi';
// const WEB_APP_ID = 'UncardWebApp';


const REGION = 'eu-west-1';
const DOMAIN = 'https://uncard.auth.eu-west-1.amazoncognito.com';
const WEB_APP_ID = "dj5rkicmdtfm3";
const USER_POOL_ID = 'eu-west-1_rr21KbDQ1';
const USER_POOL_WEB_CLIENT_ID = '48eluvep7k3sqjpjjpqr19u6cv';
const REST_API = "https://x31janv249.execute-api.eu-west-1.amazonaws.com/dev";

const AwsConfig = () => {
    // let config = AwsHelper({accessKey: ACCESS_KEY, secretKey: SECRET_KEY, region: REGION})
    const AWS_CONFIG = {
        Auth: {
            region: REGION,
            userPoolId: USER_POOL_ID,
            userPoolWebClientId: USER_POOL_WEB_CLIENT_ID,
            authenticationFlowType: 'USER_PASSWORD_AUTH',
            mandatorySignIn: true,
            oauth: {
                domain: DOMAIN,
                scope: ['email', 'profile', 'openid', 'aws.cognito.signin.user.admin'],
                redirectSignIn: `https://master.${WEB_APP_ID}.amplifyapp.com/`,
                redirectSignOut: `https://master.${WEB_APP_ID}.amplifyapp.com/`,
                responseType: 'code'
            }
        },
        API: {
            endpoints: [
                {
                    name: "TestGatewayAPI",
                    endpoint: REST_API,
                    // custom_header: async () => {
                    //     // return { Authorization : 'token' }
                    //     // Alternatively, with Cognito User Pools use this:
                    //     return { Authorization: `Bearer ${(await Auth.currentSession()).getAccessToken().getJwtToken()}` }
                    //     // return { Authorization: `Bearer ${(await Auth.currentSession()).getIdToken().getJwtToken()}` }
                    // }
                },
            ]
        }
    };
    // console.log("AWS_CONFIG: ", AWS_CONFIG)
    Amplify.configure(AWS_CONFIG);
}

export default AwsConfig