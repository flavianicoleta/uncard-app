import React from 'react';
import './App.css';

import {AuthState, onAuthUIStateChange} from '@aws-amplify/ui-components';
import AwsConfig from "./AwsConfig";
import { Login, Main } from "./components/index";

const AuthStateApp = () => {
    AwsConfig();

    const [authState, setAuthState] = React.useState();
    const [user, setUser] = React.useState();

    React.useEffect(() => {
        return onAuthUIStateChange((nextAuthState, authData) => {
            setAuthState(nextAuthState);
            setUser(authData)
            // console.log("userPoolId: " + userPoolId)
            // console.log(authData)
        });
    }, []);
    return authState === AuthState.SignedIn && user ? (
        <Main/>
    ) : (
        <Login/>
    );
};

export default AuthStateApp;
